FROM osrf/ros:melodic-desktop-full-bionic

ARG uid=1000
ARG gid=1000

COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENV DEBIAN_FRONTEND noninteractive
ENV USERNAME ros
RUN useradd -m $USERNAME && \
        echo "$USERNAME:$USERNAME" | chpasswd && \
        usermod --shell /bin/bash $USERNAME && \
        usermod -aG sudo $USERNAME && \
        echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$USERNAME && \
        chmod 0440 /etc/sudoers.d/$USERNAME && \
        usermod  --uid $uid $USERNAME && \
        groupmod --gid $gid $USERNAME && \
        usermod -aG video $USERNAME

RUN apt-get update && apt-get upgrade -y && \
        apt-get install -y \
          python-catkin-tools \
          bash \
          mesa-utils \
          libgl1-mesa-glx \
          apt-utils
WORKDIR /home/$USERNAME
COPY src /home/$USERNAME/src
RUN rosdep install --from-paths . --ignore-src -r -y
RUN catkin config \
      --extend /opt/ros/melodic && \
    catkin build

ENTRYPOINT ["/entrypoint.sh"]
