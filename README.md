# A Complete Panda Gazebo Workspace

## Important Remarks:

- Much of the code in the contained submodules is based on the information [Erdal Pekel's Blog](https://erdalpekel.de/). **Thanks, Erdal!**

## Cloning the repository

The repository contains submodules that must also be cloned
- as a guest: `git clone --recurse-submodules https://git-st.inf.tu-dresden.de/ceti/ros/mpm4cps2020.git workspace`
- as a project member with a registered ssh key: `git clone --recurse-submodules git@git-st.inf.tu-dresden.de:ceti/ros/mpm4cps.git workspace`

## Running the use case

The use case can be executed directly (with all requirements installed) or using docker-compose.
Additionally, a combination of both approaches is also possible.

### Running the use case with docker-compose

To quickly run the demo, you can use *docker-compose*.
Since the use case includes X11 GUI applications, currently only Linux is supported.


### Running the use case directly


## Requirements for development

- Ubuntu 18.4
- ROS Melodic

