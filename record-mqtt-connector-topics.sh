rosbag record -o mqtt-connector-recording \
    /panda_mqtt_connector/max_velocity \
    /panda_mqtt_connector/mqtt_message \
    /panda_mqtt_connector/next_step \
    /panda_mqtt_connector/panda/EndEffector \
    /panda_mqtt_connector/panda/LeftFinger \
    /panda_mqtt_connector/panda/Link0 \
    /panda_mqtt_connector/panda/Link1 \
    /panda_mqtt_connector/panda/Link2 \
    /panda_mqtt_connector/panda/Link3 \
    /panda_mqtt_connector/panda/Link4 \
    /panda_mqtt_connector/panda/Link5 \
    /panda_mqtt_connector/panda/Link6 \
    /panda_mqtt_connector/panda/RightFinger \
    /panda_mqtt_connector/robot_in_zone \
    /panda_mqtt_connector/trajectory_update \
    /panda_mqtt_connector/velocity \
    /panda_mqtt_connector/velocity_smoothed

