#!/bin/bash

# http://wiki.ros.org/docker/Tutorials/GUI

docker build . -t ceti-panda-gazebo-workspace \
    --build-arg UID=$UID \
    --build-arg GID=$GID

